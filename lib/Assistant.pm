# ClamTk, copyright (C) 2004-2019 Dave M
#
# This file is part of ClamTk
# https://dave-theunsub.github.io/clamtk
#
# ClamTk is free software; you can redistribute it and/or modify it
# under the terms of either:
#
# a) the GNU General Public License as published by the Free Software
# Foundation; either version 1, or (at your option) any later version, or
#
# b) the "Artistic License".
package ClamTk::Assistant;

use Glib 'TRUE', 'FALSE';

# use strict;
# use warnings;
$| = 1;

use File::Copy 'copy';
use Locale::gettext;

my $pref;
my $apply_bar;

sub show_window {
    my $box = Gtk2::VBox->new( FALSE, 0 );
    $box->set_border_width( 12 );

    # Get current update preference
    $pref = ClamTk::Prefs->get_preference( 'Update' );
    # Just in case, set it to 'shared' if nothing else:
    $pref ||= 'shared';

    #<<<
    my $label = Gtk2::Label->new(
          _(
           'Please choose how you will update your antivirus signatures'
          )
    );
    my $flabel = Gtk2::Label->new;
    $flabel->set_markup( "<b>" . $label->get_label . "</b>" );
    $flabel->set_alignment( 0.0, 0.5 );
    #>>>
    $box->pack_start( $flabel, FALSE, FALSE, 10 );

    my $bbox = Gtk2::VButtonBox->new;
    $bbox->can_focus( FALSE );
    $bbox->set_spacing_default( 5 );
    $bbox->set_layout_default( 'start' );
    $box->add( $bbox );

    #<<<
    my $auto_button = Gtk2::RadioButton->new_with_label_from_widget(
            undef,
            _('My computer automatically receives updates')
    );
    $auto_button->can_focus( FALSE );
    $auto_button->signal_connect(
        toggled => sub {
            if( $auto_button->get_active ) {
                $pref = 'shared';
            } else {
                $pref = 'single';
            }
        }
    );

    my $man_button = Gtk2::RadioButton->new_from_widget( $auto_button );
    $man_button->set_label(
            _('I would like to update signatures myself')
    );
    $man_button->can_focus( FALSE );
    $man_button->signal_connect(
        toggled => sub {
            if( $man_button->get_active ) {
                $pref = 'single';
            } else {
                $pref = 'shared';
            }
         }
    );
    #>>>
    $bbox->pack_start( $auto_button, FALSE, FALSE, 0 );
    $bbox->pack_start( $man_button,  FALSE, FALSE, 0 );

    if ( $pref eq 'shared' ) {
        $auto_button->set_active( TRUE );
    } elsif ( $pref eq 'single' ) {
        $man_button->set_active( TRUE );
    }

    create_infobar();
    $box->pack_start( $apply_bar, FALSE, FALSE, 10 );

    add_infobar_label( _( 'Press Apply to save changes' ) );
    add_infobar_button_and_signal();

    $box->show_all();
    return $box;
}

sub create_infobar {
    $apply_bar = Gtk2::HButtonBox->new;
}

sub add_infobar_label {
    my $text = shift;

    my $label = Gtk2::Label->new;
    $label->set_use_markup( TRUE );
    $label->set_text( $text );

    $apply_bar->add( $label );
}

sub add_infobar_button_and_signal {
    my $button = Gtk2::Button->new_from_stock( 'gtk-apply' );
    $button->signal_connect(
        clicked => sub {
            Gtk2->main_iteration while Gtk2->events_pending;
            if ( save() ) {
                if ( save() ) {
                    set_infobar_text( TRUE );
                } else {
                    set_infobar_text( FALSE );
                }
            }
        }
    );

    $apply_bar->add( $button );
    $apply_bar->show_all;
}

sub set_infobar_text {
    # Don't really need the $bar from @_ anymore...
    my ( $success, $bar ) = @_;

    # The text we display
    my $label;
    # The message type of infobar we display
    my $type;

    if ( $success ) {
        $label = _( 'Your changes were saved.' );
        $type  = 'info';
    } else {
        $label = _( 'Error updating: try again later' );
        $type  = 'error';
    }

    for my $c ( $apply_bar->get_children ) {
        if ( $c->isa( 'Gtk2::Label' ) ) {
            $c->set_text( $label );
        }
    }

    my $loop = Glib::MainLoop->new;
    Glib::Timeout->add(
        2000,
        sub {
            $loop->quit;
            FALSE;
        }
    );
    $loop->run;

    for my $c ( $apply_bar->get_children ) {
        if ( $c->isa( 'Gtk2::Label' ) ) {
            $c->set_text( _( 'Press Apply to save changes' ) );
        }
    }

    Gtk2->main_iteration while Gtk2->events_pending;
}

sub save {
    my ( $ret ) = ClamTk::Prefs->set_preference( 'Update', $pref );

    if ( $ret == 1 ) {
        # It worked, so see if there are system signatures around
        # we can copy to save bandwidth and time
        my $paths = ClamTk::App->get_path( 'db' );

        if ( $pref eq 'single' ) {
            my ( $d, $m ) = ( 0 ) x 2;
            Gtk2->main_iteration while ( Gtk2->events_pending );
            for my $dir_list (
                '/var/clamav',             '/var/lib/clamav',
                '/opt/local/share/clamav', '/usr/share/clamav',
                '/usr/local/share/clamav', '/var/db/clamav',
                )
            {
                if ( -e "$dir_list/daily.cld" ) {
                    copy( "$dir_list/daily.cld", "$paths/daily.cld" );
                    $d = 1;
                } elsif ( -e "$dir_list/daily.cvd" ) {
                    copy( "$dir_list/daily.cvd", "$paths/daily.cvd" );
                    $d = 1;
                }
                if ( -e "$dir_list/main.cld" ) {
                    copy( "$dir_list/main.cld", "$paths/main.cld" );
                    $m = 1;
                } elsif ( -e "$dir_list/main.cvd" ) {
                    copy( "$dir_list/main.cvd", "$paths/main.cvd" );
                    $m = 1;
                }
                if ( -e "$dir_list/bytecode.cld" ) {
                    copy( "$dir_list/bytecode.cld", "$paths/bytecode.cld" );
                }
                last if ( $d && $m );
            }
        }
    }
    # Update statusbar
    Gtk2->main_iteration while Gtk2->events_pending;
    ClamTk::GUI->startup();
    Gtk2->main_iteration while Gtk2->events_pending;

    return 1;
}

1;
